package ru.ama.restservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.ama.restservice.model.DeliveryTask;
import ru.ama.restservice.repository.DeliveryTaskRepository;

import java.time.LocalDate;
import java.util.Objects;

@Service
public class DeliveryTaskService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final DeliveryTaskRepository deliveryTaskRepository;
    private final PlanningService planningService;

    public DeliveryTaskService(DeliveryTaskRepository deliveryTaskRepository,
                               PlanningService planningService) {
        this.deliveryTaskRepository = deliveryTaskRepository;
        this.planningService = planningService;
    }

    public void addDeliveryTaskOnPlanning(DeliveryTask deliveryTask) {
        deliveryTaskRepository.save(deliveryTask);
        logger.debug("saved task with id = {}", deliveryTask.getId());

        if (Objects.equals(deliveryTask.getMeetDate().toLocalDate(), LocalDate.now())) {
            planningService.sendToPlanning(deliveryTask.getId());
            logger.debug("sent task on planning");
        }
    }
}
