package ru.ama.restservice.ws.util;

import org.springframework.http.ResponseEntity;
import ru.ama.restservice.ws.dto.ErrorResponse;

import java.time.LocalDateTime;

public class ResponseBuilder {
    public static ResponseEntity<ErrorResponse> buildErrorResponse(String msg, int status) {
        ErrorResponse response = new ErrorResponse();

        response.setMessage(msg);
        response.setTimeStamp(LocalDateTime.now());

        return ResponseEntity.status(status).body(response);
    }
}
