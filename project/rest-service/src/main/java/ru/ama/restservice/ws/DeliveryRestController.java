package ru.ama.restservice.ws;

import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.ama.restservice.model.DeliveryTask;
import ru.ama.restservice.service.DeliveryTaskService;
import ru.ama.restservice.service.EmployeeService;
import ru.ama.restservice.ws.dto.DeliveryTaskDto;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping(value = "/delivery", produces = "application/json")
public class DeliveryRestController extends AbstractController {
    private final DeliveryTaskService deliveryTaskService;
    private final EmployeeService employeeService;
    private final ConversionService conversionService;

    public DeliveryRestController(DeliveryTaskService deliveryTaskService,
                                  EmployeeService employeeService,
                                  ConversionService conversionService) {
        this.deliveryTaskService = deliveryTaskService;
        this.employeeService = employeeService;
        this.conversionService = conversionService;
    }

    @PostMapping(value = "/tasks")
    public ResponseEntity<Void> addDeliveryTask(@RequestBody @Valid DeliveryTaskDto dto) {
        deliveryTaskService.addDeliveryTaskOnPlanning(conversionService.convert(dto, DeliveryTask.class));
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/tasks/{employee-id}")
    public ResponseEntity<List<DeliveryTaskDto>> getScheduledTasks(@Min(1) @PathVariable("employee-id") Long employeeId) {
        return ResponseEntity.ok(employeeService.getScheduledTasksForEmployee(employeeId).stream()
                .map(et -> conversionService.convert(et, DeliveryTaskDto.class))
                .collect(Collectors.toList()));
    }
}
