package ru.ama.restservice.ws;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.ama.restservice.ws.dto.ErrorResponse;

import static ru.ama.restservice.ws.util.ResponseBuilder.buildErrorResponse;

public abstract class AbstractController {
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArgumentException(IllegalArgumentException e) {
        return buildErrorResponse(e.getMessage(), 404);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> handleConstraintViolationException(ConstraintViolationException e) {
        return buildErrorResponse(e.getMessage(), 400);
    }

    @ExceptionHandler(NoSuchMethodException.class)
    public ResponseEntity handleNoSuchMethodException(NoSuchMethodException e) {
        return buildErrorResponse("invalid data type: " + e.getMessage(), 400);
    }
}
