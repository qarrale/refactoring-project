package ru.ama.restservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.ama.restservice.model.Schedule;
import ru.ama.restservice.model.ScheduleInterval;
import ru.ama.restservice.repository.EmployeeRepository;
import ru.ama.restservice.ws.dto.EmployeeScheduleDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmployeeScheduleDtoToSchedule implements Converter<EmployeeScheduleDto, Schedule> {
    private final EmployeeRepository repository;

    public EmployeeScheduleDtoToSchedule(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public Schedule convert(EmployeeScheduleDto source) {
        Schedule schedule = new Schedule();

        schedule.setAuthor(source.getAuthor());
        schedule.setDate(source.getDate());

        List<ScheduleInterval> intervals = new ArrayList<>(source.getIntervals().size());

        source.getIntervals().forEach(interval -> {
            ScheduleInterval scheduleInterval = new ScheduleInterval();
            scheduleInterval.setEmployee(repository.findById(interval.getEmployeeId()).orElseThrow());
            scheduleInterval.setStart(interval.getStart());
            scheduleInterval.setEnd(interval.getEnd());
            scheduleInterval.setSchedule(schedule);
            intervals.add(scheduleInterval);
        });

        schedule.setIntervalList(intervals);
        return schedule;
    }
}
