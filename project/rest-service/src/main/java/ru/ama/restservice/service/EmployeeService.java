package ru.ama.restservice.service;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import ru.ama.restservice.model.DeliveryTask;
import ru.ama.restservice.model.Schedule;
import ru.ama.restservice.model.ScheduleInterval;
import ru.ama.restservice.repository.EmployeeRepository;
import ru.ama.restservice.repository.ScheduleRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final ScheduleRepository scheduleRepository;

    public EmployeeService(EmployeeRepository employeeRepository,
                           ScheduleRepository scheduleRepository) {
        this.employeeRepository = employeeRepository;
        this.scheduleRepository = scheduleRepository;
    }

    public List<DeliveryTask> getScheduledTasksForEmployee(Long employeeId) {
        Schedule currentSchedule = scheduleRepository.getScheduleByDate(LocalDate.now());

        Assert.notNull(currentSchedule, "there is no active schedule on today");

        return currentSchedule.getIntervalList().stream()
                .filter(interval -> Objects.equals(interval.getEmployee().getId(), employeeId))
                .map(ScheduleInterval::getDeliveryTask)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
