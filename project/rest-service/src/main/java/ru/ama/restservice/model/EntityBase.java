package ru.ama.restservice.model;

public abstract class EntityBase <T> {
    protected T id;

    public void setId(T id) {
        this.id = id;
    }

    public T getId() {
        return id;
    }
}
