package ru.ama.restservice.ws.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmployeeScheduleDto {
    @NotNull
    private LocalDate date;
    @NotNull
    private String author;
    @NotNull
    private List<ScheduleInterval> intervals;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public List<ScheduleInterval> getIntervals() {
        return intervals;
    }

    public void setIntervals(List<ScheduleInterval> intervals) {
        this.intervals = intervals;
    }

    public static class ScheduleInterval {
        @NotNull
        private Long employeeId;
        @NotNull
        private LocalDateTime start;
        @NotNull
        private LocalDateTime end;

        public Long getEmployeeId() {
            return employeeId;
        }

        public void setEmployeeId(Long employeeId) {
            this.employeeId = employeeId;
        }

        public LocalDateTime getStart() {
            return start;
        }

        public void setStart(LocalDateTime start) {
            this.start = start;
        }

        public LocalDateTime getEnd() {
            return end;
        }

        public void setEnd(LocalDateTime end) {
            this.end = end;
        }
    }
}


