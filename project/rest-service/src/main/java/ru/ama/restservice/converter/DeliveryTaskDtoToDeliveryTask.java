package ru.ama.restservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.ama.restservice.model.DeliveryTask;
import ru.ama.restservice.ws.dto.DeliveryTaskDto;

@Component
public class DeliveryTaskDtoToDeliveryTask implements Converter<DeliveryTaskDto, DeliveryTask> {
    @Override
    public DeliveryTask convert(DeliveryTaskDto source) {
        DeliveryTask task = new DeliveryTask();

        task.setMeetDate(source.getMeetDate());
        task.setAddress(source.getAddress());
        task.setClient(source.getClient());

        return task;
    }
}
