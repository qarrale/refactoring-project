package ru.ama.restservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.ama.restservice.model.DeliveryTask;
import ru.ama.restservice.ws.dto.DeliveryTaskDto;

@Component
public class DeliveryTaskToDeliveryTaskDto implements Converter<DeliveryTask, DeliveryTaskDto> {
    @Override
    public DeliveryTaskDto convert(DeliveryTask source) {
        DeliveryTaskDto taskDto = new DeliveryTaskDto();

        taskDto.setMeetDate(source.getMeetDate());
        taskDto.setAddress(source.getAddress());
        taskDto.setClient(source.getClient());

        return taskDto;
    }
}

