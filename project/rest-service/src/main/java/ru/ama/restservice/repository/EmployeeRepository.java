package ru.ama.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ama.restservice.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
