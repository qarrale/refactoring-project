package ru.ama.restservice.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.ama.restservice.model.Schedule;
import ru.ama.restservice.ws.dto.EmployeeScheduleDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduleToEmployeeScheduleDto implements Converter<Schedule, EmployeeScheduleDto> {
    @Override
    public EmployeeScheduleDto convert(Schedule source) {
        EmployeeScheduleDto dto = new EmployeeScheduleDto();

        dto.setAuthor(source.getAuthor());
        dto.setDate(source.getDate());

        List<EmployeeScheduleDto.ScheduleInterval> intervals = new ArrayList<>(source.getIntervalList().size());

        source.getIntervalList().forEach(interval -> {
            EmployeeScheduleDto.ScheduleInterval scheduleInterval = new EmployeeScheduleDto.ScheduleInterval();
            scheduleInterval.setEmployeeId(interval.getEmployee().getId());
            scheduleInterval.setStart(interval.getStart());
            scheduleInterval.setEnd(interval.getEnd());
            intervals.add(scheduleInterval);
        });

        dto.setIntervals(intervals);
        return dto;
    }
}
