package ru.ama.restservice.repository;

import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.PathBuilder;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import ru.ama.restservice.model.QSchedule;
import ru.ama.restservice.model.Schedule;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.Optional;

@Repository
public class ScheduleRepository extends SimpleJpaRepository<Schedule, Long> {
    private final JPAQueryFactory factory;
    private final Querydsl querydsl;

    public ScheduleRepository(EntityManager entityManager) {
        super(Schedule.class, entityManager);
        this.factory = new JPAQueryFactory(entityManager);
        this.querydsl = new Querydsl(entityManager,
                new PathBuilder<>(Schedule.class, "schedule"));
    }

    public Schedule getScheduleByDate(LocalDate date) {
        QSchedule schedule = QSchedule.schedule;

        return factory.select(schedule)
                .from(schedule)
                .where(getPredicate(schedule, date))
                .fetchFirst();
    }

    private Predicate getPredicate(QSchedule schedule, LocalDate issueDate) {
        BooleanExpression datePredicate = Optional.ofNullable(issueDate)
                .map(date -> schedule.date.eq(issueDate))
                .orElse(null);

        return ExpressionUtils.allOf(datePredicate);
    }
}
