package ru.ama.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ama.restservice.model.ScheduleInterval;

public interface ScheduleIntervalRepository extends JpaRepository<ScheduleInterval, Long> {
}
