package ru.ama.restservice.ws;

import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ama.restservice.model.Schedule;
import ru.ama.restservice.service.EmployeeScheduleService;
import ru.ama.restservice.ws.dto.EmployeeScheduleDto;

import javax.validation.Valid;

@Validated
@RestController
@RequestMapping(value = "/employees", produces = "application/json")
public class EmployeeRestController extends AbstractController {
    private final EmployeeScheduleService employeeScheduleService;
    private final ConversionService conversionService;

    public EmployeeRestController(EmployeeScheduleService employeeScheduleService,
                                  ConversionService conversionService) {
        this.employeeScheduleService = employeeScheduleService;
        this.conversionService = conversionService;
    }

    @PutMapping(value = "/schedule")
    public ResponseEntity<Void> uploadEmployeesSchedule(@RequestBody @Valid EmployeeScheduleDto employeeScheduleDto) {
        employeeScheduleService.uploadEmployeesSchedule(conversionService.convert(employeeScheduleDto, Schedule.class));
        return ResponseEntity.ok().build();
    }
}
