package ru.ama.restservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ama.restservice.model.DeliveryTask;

public interface DeliveryTaskRepository extends JpaRepository<DeliveryTask, Long> {
}
