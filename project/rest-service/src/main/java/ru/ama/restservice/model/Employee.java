package ru.ama.restservice.model;

import javax.persistence.*;

@Entity
@Table(name = "employee")
public class Employee extends EntityBase<Long> {
    private String firstName;
    private String surname;

    @Id
    @Override
    @GeneratedValue
    public Long getId() {
        return id;
    }

    @Column(name = "firstname")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "surname")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
