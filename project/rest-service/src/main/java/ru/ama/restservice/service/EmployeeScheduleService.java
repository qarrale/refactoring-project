package ru.ama.restservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.ama.restservice.model.Schedule;
import ru.ama.restservice.repository.ScheduleRepository;

import java.util.Optional;

@Service
public class EmployeeScheduleService {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ScheduleRepository scheduleRepository;

    public EmployeeScheduleService(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    public void uploadEmployeesSchedule(Schedule schedule) {
        Optional.ofNullable(scheduleRepository.getScheduleByDate(schedule.getDate()))
                .orElseThrow(() -> new IllegalArgumentException("There is active schedule on this date"));

        scheduleRepository.save(schedule);
        logger.debug("new employee schedule saved on {}", schedule.getDate());
    }
}
