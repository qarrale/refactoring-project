package ru.ama.autoplanner.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "schedule")
public class Schedule extends EntityBase<Long> {
    private LocalDate date;
    private List<ScheduleInterval> intervalList;

    @Id
    @Override
    @GeneratedValue
    public Long getId() {
        return id;
    }

    @Column(name = "date")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @OneToMany(mappedBy = "schedule")
    public List<ScheduleInterval> getIntervalList() {
        return intervalList;
    }

    public void setIntervalList(List<ScheduleInterval> intervalList) {
        this.intervalList = intervalList;
    }
}
