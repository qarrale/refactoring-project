package ru.ama.autoplanner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ama.autoplanner.model.ScheduleInterval;

import java.time.LocalDate;
import java.util.List;

public interface ScheduleIntervalRepository extends JpaRepository<ScheduleInterval, Long> {
    @Query("select interval from ScheduleInterval interval join fetch Schedule schedule " +
            "where schedule.date = :date and interval.employee is not null and interval.deliveryTask is null ")
    List<ScheduleInterval> getFreeScheduleIntervalsByDate(@Param("date") LocalDate date);

    @Query("select interval from ScheduleInterval interval join fetch Schedule schedule " +
            "where schedule.date = :date and interval.employee is not null and interval.deliveryTask is not null ")
    List<ScheduleInterval> getBusyIntervalsByDate(@Param("date") LocalDate date);
}
