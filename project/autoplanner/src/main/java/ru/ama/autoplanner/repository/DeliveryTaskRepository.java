package ru.ama.autoplanner.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ama.autoplanner.model.DeliveryTask;

public interface DeliveryTaskRepository extends JpaRepository<DeliveryTask, Long> {
}
