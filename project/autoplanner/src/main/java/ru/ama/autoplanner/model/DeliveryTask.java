package ru.ama.autoplanner.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "delivery_task")
public class DeliveryTask extends EntityBase<Long> {
    private String address;
    private LocalDateTime meetDate;
    private ScheduleInterval scheduleInterval;

    @Id
    @Override
    @GeneratedValue
    public Long getId() {
        return id;
    }

    @Column(name = "address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Column(name = "meet_date")
    public LocalDateTime getMeetDate() {
        return meetDate;
    }

    public void setMeetDate(LocalDateTime deadline) {
        this.meetDate = deadline;
    }

    @OneToOne
    @JoinColumn(name = "schedule_interval_id")
    public ScheduleInterval getScheduleInterval() {
        return scheduleInterval;
    }

    public void setScheduleInterval(ScheduleInterval scheduleInterval) {
        this.scheduleInterval = scheduleInterval;
    }
}
