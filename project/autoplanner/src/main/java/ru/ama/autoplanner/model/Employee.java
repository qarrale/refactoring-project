package ru.ama.autoplanner.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee extends EntityBase<Long> {
    @Id
    @Override
    @GeneratedValue
    public Long getId() {
        return id;
    }
}
