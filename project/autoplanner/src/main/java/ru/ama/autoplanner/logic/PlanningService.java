package ru.ama.autoplanner.logic;

import com.google.maps.model.LatLng;
import org.springframework.stereotype.Service;
import ru.ama.autoplanner.model.DeliveryTask;
import ru.ama.autoplanner.model.Employee;
import ru.ama.autoplanner.model.ScheduleInterval;
import ru.ama.autoplanner.repository.DeliveryTaskRepository;
import ru.ama.autoplanner.repository.ScheduleIntervalRepository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlanningService {
    private final DeliveryTaskRepository deliveryTaskRepository;
    private final ScheduleIntervalRepository scheduleIntervalRepository;
    private final CoordinateService coordinateService;

    public PlanningService(DeliveryTaskRepository deliveryTaskRepository,
                           ScheduleIntervalRepository scheduleIntervalRepository,
                           CoordinateService coordinateService) {
        this.deliveryTaskRepository = deliveryTaskRepository;
        this.scheduleIntervalRepository = scheduleIntervalRepository;
        this.coordinateService = coordinateService;
    }

    @Transactional
    public void performTodayAdditionalPlanning(Long taskId) {
        LocalDate currentDate = LocalDate.now();
        performTaskPlanning(taskId, currentDate);
    }

    public void performDefaultPlanning(List<Long> ids) {
        LocalDate currentDate = LocalDate.now();
        ids.forEach(id -> performTaskPlanning(id, currentDate));
    }

    private void performTaskPlanning(Long taskId, LocalDate currentDate) {
        DeliveryTask deliveryTask = deliveryTaskRepository.getReferenceById(taskId);
        LatLng taskCoordinate = coordinateService.performCoordinateCalculationByAddress(deliveryTask.getAddress());

        List<ScheduleInterval> freeIntervals = getFreeIntervalsForTask(currentDate, deliveryTask);

        Map<Employee, Optional<ScheduleInterval>> intervalsByEmployee = getBusyIntervalsByEmployeeBeforeTask(currentDate, deliveryTask);
        Map<Employee, Double> distanceByEmployee = getDistanceByEmployee(intervalsByEmployee, taskCoordinate);

        Employee selectedEmployee = selectEmployee(distanceByEmployee);
        ScheduleInterval intervals = selectInterval(freeIntervals, selectedEmployee);

        if (intervals == null) {
            planTaskForTomorrow(deliveryTask);
        } else {
            intervals.setDeliveryTask(deliveryTask);
        }
    }

    private List<ScheduleInterval> getFreeIntervalsForTask(LocalDate date, DeliveryTask task) {
        return scheduleIntervalRepository
                .getFreeScheduleIntervalsByDate(date).stream()
                .filter(i -> i.getStart().isBefore(task.getMeetDate()))
                .filter(i -> i.getEnd().isAfter(task.getMeetDate()))
                .collect(Collectors.toList());
    }

    private Map<Employee, Optional<ScheduleInterval>> getBusyIntervalsByEmployeeBeforeTask(LocalDate date, DeliveryTask task) {
        return scheduleIntervalRepository
                .getBusyIntervalsByDate(date).stream()
                .filter(i -> i.getEnd().isBefore(task.getMeetDate()))
                .collect(Collectors.groupingBy(ScheduleInterval::getEmployee,
                        Collectors.maxBy(Comparator.comparing(ScheduleInterval::getStart))));
    }

    private Map<Employee, Double> getDistanceByEmployee(Map<Employee, Optional<ScheduleInterval>> intervalsByEmployee,
                                                        LatLng taskCoordinate) {
        Map<Employee, Double> distanceByEmployee = new HashMap<>();

        intervalsByEmployee.forEach((key, value) -> {
            LatLng coordinates = value
                    .map(i -> coordinateService.performCoordinateCalculationByAddress(i.getDeliveryTask().getAddress()))
                    .orElse(null);

            Double distance = Optional.ofNullable(coordinates)
                    .map(c -> Math.sqrt(Math.pow(c.lat - taskCoordinate.lat, 2)
                            + Math.pow(c.lng - taskCoordinate.lng, 2)))
                    .orElse(0.0);

            distanceByEmployee.put(key, distance);
        });

        return distanceByEmployee;
    }

    private Employee selectEmployee(Map<Employee, Double> distanceByEmployee) {
        return distanceByEmployee.entrySet().stream().min(Map.Entry.comparingByValue()).get().getKey();
    }

    private ScheduleInterval selectInterval(List<ScheduleInterval> freeIntervals, Employee selectedEmployee) {
        return freeIntervals.stream()
                .filter(i -> i.getEmployee().equals(selectedEmployee))
                .findFirst().orElse(null);
    }

    private void planTaskForTomorrow(DeliveryTask task) {
        task.setMeetDate(task.getMeetDate().plusDays(1));
    }
}
