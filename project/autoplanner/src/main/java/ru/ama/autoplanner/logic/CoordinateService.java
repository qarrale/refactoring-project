package ru.ama.autoplanner.logic;

import com.google.maps.model.LatLng;
import org.springframework.stereotype.Service;

@Service
public class CoordinateService {
    //Google maps api usage mock
    public LatLng performCoordinateCalculationByAddress(String address) {
        LatLng latLng = new LatLng(address.hashCode(), address.replace("a", "b").hashCode());

        return latLng;
    }
}
