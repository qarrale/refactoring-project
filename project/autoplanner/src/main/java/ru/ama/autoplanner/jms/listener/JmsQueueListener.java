package ru.ama.autoplanner.jms.listener;

import org.springframework.stereotype.Component;
import ru.ama.autoplanner.jms.dto.TaskDto;
import ru.ama.autoplanner.jms.dto.TaskListDto;
import ru.ama.autoplanner.logic.PlanningService;

import java.util.List;

@Component
public class JmsQueueListener {
    private final PlanningService planningService;

    public JmsQueueListener(PlanningService planningService) {
        this.planningService = planningService;
    }

    public void pullTodayPlanningQueue() {
        TaskDto taskDto = new TaskDto();

        taskDto.setTaskId(100L);

        planningService.performTodayAdditionalPlanning(taskDto.getTaskId());
    }

    public void pullPlanningQueue() {
        List<Long> ids = List.of(100L);

        TaskListDto taskListDto = new TaskListDto();

        taskListDto.setIds(ids);

        planningService.performDefaultPlanning(taskListDto.getIds());
    }
}
